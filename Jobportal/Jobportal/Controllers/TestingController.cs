﻿using BusinessLayer.BusinessLogic.Interface;
using DataAccessLayer.Models;
using Jobportal.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer.AbstractClass;

namespace Jobportal.Controllers
{
    public class TestingController : Controller
    {
        private readonly Testing_in _bhj_testing;
        public TestingController(Testing_in bhj_testing)
        {
            _bhj_testing = bhj_testing;
        }
        ResultMsg rslt = new ResultMsg();
        bool status;
        string msg;
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Create(BusinessLayer.AbstractClass.Testing_ab testing_ab)   // insert example
        {

            try
            {
            
                    var jj = _bhj_testing.create(testing_ab);
                    if (jj == "Success")
                    {
                        msg = rslt.save_msg;
                        status = rslt.s_status;

                    }
                    else if (jj == "Failed")
                    {
                        msg = rslt.err_msg;
                        status = rslt.err_status;

                    }
                    else
                    {
                        msg = jj;
                        status = rslt.err_status;
                    }
            }
            catch (Exception ex)
            {

                var err_msg = ex;
                msg = rslt.err_ct;
            }


            return Ok(new { status = status, message = msg });

        }

        public IActionResult Update(BusinessLayer.AbstractClass.Testing_ab testing_ab)   // update example
        {

            try
            {

                var jj = _bhj_testing.update(testing_ab);
                if (jj == "Success")
                {
                    msg = rslt.update_msg;
                    status = rslt.s_status;

                }
                else if (jj == "Failed")
                {
                    msg = rslt.err_msg;
                    status = rslt.err_status;

                }
                else
                {
                    msg = jj;
                    status = rslt.err_status;
                }
            }
            catch (Exception ex)
            {

                var err_msg = ex;
                msg = rslt.err_ct;
            }


            return Ok(new { status = status, message = msg });

        }

        public string Delete(int id) // delete example
        {
            var jj = _bhj_testing.Delete(id);

            return jj;
        }


        public List<Testing_tbl> getAll() // view all the details
        {
              List<Testing_tbl> jj = new List<Testing_tbl>();
            try
            {
               
                    jj = _bhj_testing.getAll();
              
            }
            catch (Exception ex)
            {
                var err = ex;
            }

            return jj;

        }
        //public Testing_ab_list get_by_id(string name) // view details bsed on condition
        //{
        //    var jj = _bhj_testing.get_by_id(name);

        //    return jj;
        //}
        public List<Testing_tbl> get_by_name(string name) // view details bsed on condition
        {
            List<Testing_tbl> jj = new List<Testing_tbl>();
            try
            {

                jj = _bhj_testing.get_by_name(name);

            }
            catch (Exception ex)
            {
                var err = ex;
            }

            return jj;

        }
        public List<Testing_tbl> get_by_name_formbdy([FromBody] Testing_ab name) // view details bsed on condition with formbody model(json model condition)
        {
            List<Testing_tbl> jj = new List<Testing_tbl>();
            try
            {

                jj = _bhj_testing.get_by_name_formbdy(name);

            }
            catch (Exception ex)
            {
                var err = ex;
            }

            return jj;

        }
    }
}
