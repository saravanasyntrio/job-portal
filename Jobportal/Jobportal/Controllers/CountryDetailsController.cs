﻿using DataAccessLayer;
using DataAccessLayer.Models;
using Jobportal.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal.Controllers
{
    public class CountryDetailsController : Controller
    {
        private readonly ApplicationDbContext context;
        public CountryDetailsController(ApplicationDbContext context)
        {
            this.context = context;
        }
        ResultMsg rslt = new ResultMsg();
        bool status;
        string msg;
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult GetCurrency()
        {
          
            var currencyList = (from product in context.Currency_tbl
                                select new SelectListItem()
                                {
                                    Text = product.currency_name,
                                    Value = product.id.ToString(),
                                }).ToList();

            currencyList.Insert(0, new SelectListItem()
            {
                Text = "----Select----",
                Value = string.Empty
            });

            return Json(currencyList);
        }

        [HttpPost]
        public IActionResult CreateCountry(Country_tbl countryData)
        {
            //Country_tbl employee = new Country_tbl
            //{
            //    country_name = countryData.country_name,
            //    code = countryData.code,
            //    currency_id = countryData.currency_id,
            //    nationality= countryData.nationality,
            //    is_active= countryData.is_active

            //};
            //return Json(employee);
            string count = "";
            try
            {
                if (context.Country_tbl.Where(x => x.code.ToLower() == countryData.code.ToLower())
                                   .Select(x => x.id).FirstOrDefault() > 0)
                {
                    msg = "Country Already Exists";
                    status = rslt.err_status;
                }
                else
                {
                    Country_tbl country = new Country_tbl();
                    country.country_name = countryData.country_name;
                    country.code = countryData.code;
                    country.currency_id = countryData.currency_id;
                    country.nationality = countryData.nationality;
                    country.is_active = countryData.is_active;
                    context.Country_tbl.Add(country);
                    var count1 = context.SaveChanges();


                    count = Convert.ToString(count1);
                    if (count == "1")
                    {
                        msg = rslt.save_msg;
                        status = rslt.s_status;
                    }
                    else
                    {
                        msg = rslt.err_msg;
                        status = rslt.err_status;
                    }
                }
                

            }
            catch (Exception ex)
            {

                var err_msg = ex;
                msg = rslt.err_ct;
            }
            return Ok(new { status = status, message = msg });
        }

      
    }
}

