﻿$(document).ready(function () {
    alert("hi");
    $("#customerDatatable").DataTable({
        "processing": true,
        "serverSide": true,
        "filter": true,
        "ajax": {
            "url": "http://localhost:62332/api/Country",
            //"url": "https://localhost:44308/api/Values",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs": [{
            "targets": [0],
            "visible": false,
            "searchable": false
        }],
        
        "columns": [
            { "data": "id", "name": "Id", "autoWidth": true },
            { "data": "countryName", "name": "Country Name", "autoWidth": true },
            { "data": "code", "name": "Code", "autoWidth": true },
            { "data": "currency", "name": "Currency", "autoWidth": true },
            { "data": "nationality", "name": "Nationality", "autoWidth": true },
            { "data": "countrystatus", "name": "Country status", "autoWidth": true },
            {
                "render": function (data, row) { return "<a href='#' class='btn btn-danger' onclick=DeleteCustomer('" + row.id + "'); >Delete</a>"; }
            },
        ]
    });
});  