﻿using BusinessLayer.AbstractClass;
using BusinessLayer.BusinessLogic.Interface;
using DataAccessLayer;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLayer.BusinessLogic
{
   public class Testing_logic : Testing_in
    {
        private readonly ApplicationDbContext _context;
        public Testing_logic(ApplicationDbContext context)
        {
            _context = context;

        }
        public string create(AbstractClass.Testing_ab testing_ab)
        {
            string count = "";
            try
            {
                if (_context.Testing_tbl.Where(x => x.testing_name.ToLower() == testing_ab.testing_name.ToLower())
                                   .Select(x => x.id).FirstOrDefault() > 0)
                {
                    return "Name already exist";
                }

               
                DataAccessLayer.Models.Testing_tbl obj_testing = new DataAccessLayer.Models.Testing_tbl();
                obj_testing.testing_name = testing_ab.testing_name;


                _context.Testing_tbl.Add(obj_testing);
                var count1 = _context.SaveChanges();
                count = Convert.ToString(count1);
                if (count == "1")
                {
                    count = "Success";
                }
                else
                {
                    count = "Failed";
                }

            }
            catch
            {
                count = "Error";
            }
            return count;
        }
        public string update(AbstractClass.Testing_ab testing_ab)
        {
            string count = "";
            try
            {
                if (_context.Testing_tbl.Where(x => x.testing_name.ToLower() == testing_ab.testing_name.ToLower())
                                   .Select(x => x.id).FirstOrDefault() > 0)
                {
                    return "Name already exist";
                }


                DataAccessLayer.Models.Testing_tbl obj_testing = new DataAccessLayer.Models.Testing_tbl();
                obj_testing.id = testing_ab.id;
                obj_testing.testing_name = testing_ab.testing_name;


                _context.Testing_tbl.Update(obj_testing);
                var count1 = _context.SaveChanges();
                count = Convert.ToString(count1);
                if (count == "1")
                {
                    count = "Success";
                }
                else
                {
                    count = "Failed";
                }

            }
            catch
            {
                count = "Error";
            }
            return count;
        }

        public string Delete(int id)
        {
            string count = "";
            try
            {
                var ob_user = _context.Testing_tbl.Find(id);
                _context.Testing_tbl.Remove(ob_user);
                var count1 = _context.SaveChanges();
                count = Convert.ToString(count1);
                if (count == "1")
                {
                    count = "User Deleted Successfully !";
                }
                else
                {
                    count = "User Deletion Failed";
                }
            }
            catch
            {
                count = "Error";
            }
            return count;
        }
        public List<Testing_tbl> getAll()
        {
            return _context.Testing_tbl.ToList();
        }

        //   public Testing_ab_list get_by_id(string name) 
        public List<Testing_tbl> get_by_name(string name)
        {

            return _context.Testing_tbl
               .Where(x => x.testing_name == name).ToList();
          
        }

        public List<Testing_tbl> get_by_name_formbdy(Testing_ab name)
        {

            return _context.Testing_tbl
               .Where(x => x.testing_name == name.testing_name).ToList();
            
        }
    }
}
