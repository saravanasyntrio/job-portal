﻿using BusinessLayer.AbstractClass;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.BusinessLogic.Interface
{
   public interface Testing_in
    {
        public string create(AbstractClass.Testing_ab testing_ab);
        public string update(AbstractClass.Testing_ab testing_ab);
        public string Delete(int id);
        public List<Testing_tbl> getAll();
        // public Testing_ab_list get_by_id(string name);
        public List<Testing_tbl> get_by_name(string name);
        public List<Testing_tbl> get_by_name_formbdy(Testing_ab name);
    }
}
