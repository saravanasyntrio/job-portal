﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer
{
   public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public DbSet<Testing_tbl> Testing_tbl { get; set; }
        public DbSet<Currency_tbl> Currency_tbl { get; set; }
        public DbSet<Country_tbl> Country_tbl { get; set; }

        public DbSet<State_tbl> State_tbl { get; set; }

        public DbSet<District_tbl> District_tbl { get; set; }

        public DbSet<Location_tbl> Location_tbl { get; set; }
        public DbSet<org_cat_tbl> org_cat_tbl { get; set; }
        public DbSet<org_tbl> org_tbl { get; set; }
        public DbSet<org_cnt_tbl> org_cnt_tbl { get; set; }
        public DbSet<org_mem_tbl> org_mem_tbl { get; set; }
        public DbSet<sadmin_tbl> sadmin_tbl { get; set; }
        public DbSet<emp_tbl> emp_tbl { get; set; }
        public DbSet<seek_tbl> seek_tbl { get; set; }
        public DbSet<job_reg_tbl> job_reg_tbl { get; set; }
        public DbSet<app_job_tbl> app_job_tbl { get; set; }

        public DbSet<country_demo> country_demo { get; set; }
        public DbSet<Customer> Customer { get; set; }
    }
}
