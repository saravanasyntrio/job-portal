﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
    [Table("Country_tbl", Schema = "general")]
    public class Country_tbl
    {
        [Key]
        public int id { get; set; }
        [Column(TypeName = "varchar(50)")]
        [Required]
        public string country_name { get; set; }
        [Column(TypeName = "varchar(50)")]
        [Required]
        public string code { get; set; }
        [Required]
        [ForeignKey("currency")]
        public int currency_id { get; set; }
        [Column(TypeName = "varchar(50)")]
        [Required]
        public string nationality { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string identifier { get; set; }
        [Required]
        public Boolean is_active { get; set; }

        public Currency_tbl currency { get; set; }

    }
}

