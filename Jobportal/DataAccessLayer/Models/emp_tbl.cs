﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
    [Table("emp_tbl", Schema = "jobportal")]
    public class emp_tbl
    {
        [Key]
        public int id { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string e_name { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string email_id { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string e_password { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        [Required]
        public DateTime registration_date { get; set; }

        [Column(TypeName = "varchar(25)")]
        [Required]
        public string contact1 { get; set; }

        [Column(TypeName = "varchar(25)")]
        
        public string contact2 { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        [Required]
        public DateTime start_date { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        public DateTime end_date { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string company_website { get; set; }

        [Required]
        [ForeignKey("sadmin")]
        public int approved_by { get; set; }

        public sadmin_tbl sadmin { get; set; }

        [Required]
        public Boolean status { get; set; }

        [Column(TypeName = "varchar(100)")]
        [Required]
        public string e_tokken { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        [Required]
        public DateTime last_login { get; set; }
    }
}
