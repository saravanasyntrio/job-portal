﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
    [Table("org_tbl1", Schema = "organization")]
    public class org_tbl
    {
        [Key]
        public int org_id { get; set; }

        [Column(TypeName = "varchar(100)")]
        [Required]
        public string org_name { get; set; }


        [Column(TypeName = "varchar(20)")]
        public string short_name { get; set; }
        public int parent_id { get; set; }


        [Required]
        [ForeignKey("org_cat")]
        public int org_type_id { get; set; }

        [Required]
        [ForeignKey("Country")]
        public int country_id { get; set; }

        [Required]
        [ForeignKey("State")]
        public int state_id { get; set; }

        [Required]
        [ForeignKey("Currency")]
        public int currency_id { get; set; }

        [Required]
        [ForeignKey("District")]
        public int dist_id { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string code { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string tax_number { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string vat_number { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        public DateTime tl_expiry_date { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string c_group_name { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string comme_name { get; set; }
        public Byte cmp_img { get; set; }
        public Byte cmp_logo { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string land_mark { get; set; }

        [Column(TypeName = "varchar(25)")]
        public string lat_loc { get; set; }

        [Column(TypeName = "varchar(25)")]
        public string lng_loc { get; set; }


        [Column(TypeName = "varchar(25)")]
        public string door_no { get; set; }

        [Column(TypeName = "varchar(35)")]
        public string street_name { get; set; }

        [Column(TypeName = "varchar(35)")]
        public string loc_name { get; set; }

        [Column(TypeName = "varchar(35)")]
        public string dist { get; set; }

        [Column(TypeName = "varchar(15)")]
        public string post_no { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        public DateTime est_year { get; set; }
        public int no_branch { get; set; }

        [Required]
        public Boolean status { get; set; }

        //[Display(Name = "Country_tbl")]
        //public virtual int countryId { get; set; }

        //[ForeignKey("id")]
        //public virtual Country_tbl country { get; set; }

        //// Foreign key   
        //[Display(Name = "State_tbl")]
        //public virtual int stateId { get; set; }

        //[ForeignKey("id")]
        //public virtual State_tbl state { get; set; }
        public org_cat_tbl org_cat { get; set; }
        public Country_tbl Country { get; set; }
        public State_tbl State { get; set; }
        public Currency_tbl Currency { get; set; }
        public District_tbl District { get; set; }
    }
}
