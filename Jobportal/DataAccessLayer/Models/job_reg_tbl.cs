﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
    [Table("job_reg_tbl", Schema = "jobportal")]
    public class job_reg_tbl
    {
        [Key]
        public int id { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string job_title { get; set; }

        [Column(TypeName = "varchar(max)")]
        [Required]
        public string job_skills { get; set; }

        [Column(TypeName = "varchar(max)")]
        [Required]
        public string job_desc { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        [Required]
        public DateTime posting_date { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        [Required]
        public DateTime start_date { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        [Required]
        public DateTime end_date { get; set; }

        [Required]
        [ForeignKey("state")]
        public int state_fk { get; set; }

        [Required]
        [ForeignKey("states")]
        public int desired_state_fk { get; set; }

        [Required]
        [ForeignKey("emp")]
        public int employeer_id { get; set; }

        public State_tbl state { get; set; }

        public State_tbl states { get; set; }
        public emp_tbl emp { get; set; }

    }
}
