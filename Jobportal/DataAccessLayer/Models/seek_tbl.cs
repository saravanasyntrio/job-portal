﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
    [Table("seek_tbl", Schema = "jobportal")]
    public class seek_tbl
    {
        [Key]
        public int id { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string s_name { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string email_id { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string e_password { get; set; }

        [Column(TypeName = "varchar(10)")]
        [Required]
        public string gender { get; set; }

        [Required]
        public int seeker_type { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        [Required]
        public DateTime registration_date { get; set; }

        [Column(TypeName = "varchar(25)")]
        [Required]
        public string contact1 { get; set; }

        [Column(TypeName = "varchar(25)")]
        
        public string contact2 { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        [Required]
        public DateTime start_date { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        public DateTime end_date { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string DOB { get; set; }

        [Required]
        [ForeignKey("sadmin")]
        public int approved_by { get; set; }

        public sadmin_tbl sadmin { get; set; }

        [Required]
        public Boolean status { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string e_tokken { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        [Required]
        public DateTime last_login { get; set; }

        [Column(TypeName = "varchar(max)")]
        [Required]
        public string s_resume { get; set; }

        [Required]
        public float s_experience { get; set; }

        [Column(TypeName = "varchar(250)")]
        [Required]
        public string key_skills { get; set; }

        [Column(TypeName = "varchar(250)")]
        [Required]
        public string profile_heading { get; set; }

        [Required]
        public int higher_qualification { get; set; }

        [Required]
        public Boolean allow_sms { get; set; }

        [Required]
        public Boolean allow_email { get; set; }

        [Column(TypeName = "varchar(max)")]
        public bool face_img { get; set; }

        [Required]
        [ForeignKey("state")]
        public int state_fk { get; set; }

        

        [Required]
        [ForeignKey("District")]
        public int dist_fk { get; set; }



        [Required]
        [ForeignKey("states")]
        public int desired_state_fk { get; set; }

        [Required]
        [ForeignKey("Districts")]
        public int desired_dist_fk { get; set; }

        public State_tbl state { get; set; }
        public State_tbl states { get; set; }

        public District_tbl District { get; set; }

        public District_tbl Districts { get; set; }



    }
}
