﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
    [Table("Currency_tbl", Schema = "general")]
    public class Currency_tbl
    {
        [Key]
        public int id { get; set; }
        [Column(TypeName = "varchar(50)")]
        [Required]
        public string currency_name { get; set; }
        [Column(TypeName = "varchar(20)")]
        [Required]
        public string code { get; set; }

        public bool symbol { get; set; }

        [Required]
        public Boolean is_default { get; set; }

        [Required]
        public Boolean is_active { get; set; }

    }
}
