﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
    
    [Table("country_demo", Schema = "general")]
    public class country_demo
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
        public string Code { get; set; }
        public string Currency { get; set; }
        public string Nationality { get; set; }
        public string Countrystatus { get; set; }
    }
}
