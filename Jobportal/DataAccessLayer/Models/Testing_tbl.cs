﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
    [Table("Testing_tbl", Schema = "general")]
    public class Testing_tbl
    {
        [Key]
        public int id { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string testing_name { get; set; }
        
    }
}
