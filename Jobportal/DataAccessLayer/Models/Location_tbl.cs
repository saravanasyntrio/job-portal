﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
    [Table("Location_tbl", Schema = "general")]
    public class Location_tbl
    {
        [Key]
        public int id { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string location_name { get; set; }


        [Column(TypeName = "varchar(20)")]
        [Required]
        public string code { get; set; }


        [Required]
        [ForeignKey("district")]
        public int province_area_id { get; set; }


        [Column(TypeName = "varchar(50)")]
        public string identifier { get; set; }

        [Required]
        public Boolean is_active { get; set; }

        public District_tbl district { get; set; }
    }
}
