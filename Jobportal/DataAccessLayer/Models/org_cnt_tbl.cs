﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
    [Table("org_cnt_tbl", Schema = "organization")]
    public class org_cnt_tbl
    {
        [Key]
        public int org_c_id { get; set; }

        [Required]
        [ForeignKey("org")]
        public int org_id { get; set; }

        [Column(TypeName = "varchar(25)")]
        public string con_number1 { get; set; }

        [Column(TypeName = "varchar(25)")]
        public string con_number2 { get; set; }

        [Column(TypeName = "varchar(35)")]
        public string con_email1 { get; set; }

        [Column(TypeName = "varchar(35)")]
        public string con_email2 { get; set; }

        [Column(TypeName = "varchar(25)")]
        public string whatsapp_no1 { get; set; }

        [Column(TypeName = "varchar(25)")]
        public string whatsapp_no2 { get; set; }

        [Column(TypeName = "varchar(35)")]
        public string twitter_acc { get; set; }

        [Column(TypeName = "varchar(35)")]
        public string linked_in_acc { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string website_address { get; set; }

        public int total_employee { get; set; }
        public int total_board_mem { get; set; }

        public Boolean org_closed { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        public DateTime org_closed_date { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string org_closed_reason { get; set; }
        public Boolean org_inactive { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        public DateTime org_inactive_date { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string org_inactive_reason { get; set; }

        [Column(TypeName = "varchar(250)")]
        public string Description { get; set; }


        public org_tbl org { get; set; }
    }
}
