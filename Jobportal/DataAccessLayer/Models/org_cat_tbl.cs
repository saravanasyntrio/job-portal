﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace DataAccessLayer.Models
{
    [Table("org_cat_tbl", Schema = "organization")]
    public class org_cat_tbl
    {
        [Key]
        public int id { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string category_name { get; set; }

        [Required]
        public Boolean is_active { get; set; }

        
    }
}
