﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
    [Table("sadmin_tbl", Schema = "jobportal")]
    public class sadmin_tbl
    {
        [Key]
        public int id { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string name { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string email_id { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string a_password { get; set; }

        [Column(TypeName = "varchar(25)")]
        [Required]
        public string employee_type { get; set; }

        [Column(TypeName = "varchar(25)")]
        [Required]
        public string contact1 { get; set; }

        [Column(TypeName = "varchar(25)")]
        [Required]
        public string contact2 { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        [Required]
        public DateTime start_date { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        public DateTime end_date { get; set; }

        [Required]
        public Boolean status { get; set; }

        [Column(TypeName = "varchar(100)")]
        [Required]
        public string a_token { get; set; }


        //[Column(TypeName = "timestamp without time zone")]
        [Required]
        public DateTime last_login { get; set; }

    }
}
