﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
    [Table("app_job_tbl", Schema = "jobportal")]
    public class app_job_tbl
    {
        [Key]
        public int id { get; set; }

        [Required]
        [ForeignKey("emp")]
        public int employee_id { get; set; }

        public emp_tbl emp { get; set; }

        [Required]
        [ForeignKey("job")]
        public int job_id_fk { get; set; }

        public job_reg_tbl job { get; set; }

        //[Column(TypeName = "timestamp without time zone")]
        [Required]
        public DateTime date_applied { get; set; }
    }
}
