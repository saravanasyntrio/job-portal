﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
    [Table("State_tbl", Schema = "general")]
    public class State_tbl
    {
        [Key]
        public int id { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string state_name { get; set; }


        [Column(TypeName = "varchar(20)")]
        [Required]
        public string code { get; set; }


        [Required]
        [ForeignKey("country")]
        public int country_id { get; set; }

       
        [Column(TypeName = "varchar(50)")]
        public string identifier { get; set; }
        
        [Required]
        public Boolean is_active { get; set; }

        public Country_tbl country { get; set; }
    }
}
