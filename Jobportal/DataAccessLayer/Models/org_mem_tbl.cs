﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
    [Table("org_mem_tbl", Schema = "organization")]
    public class org_mem_tbl
    {
        [Key]
        public int org_m_id { get; set; }

        [Required]
        [ForeignKey("org")]
        public int org_id { get; set; }

        [Required]
        [ForeignKey("org_cnt")]
        public int org_c_id { get; set; }

        public org_tbl org { get; set; }
        public org_cnt_tbl org_cnt { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string stake_holder { get; set; }

        [Column(TypeName = "varchar(20)")]
        public string short_name { get; set; }

        [Column(TypeName = "varchar(25)")]
        public string con_number1 { get; set; }

        [Column(TypeName = "varchar(25)")]
        public string con_number2 { get; set; }

        [Column(TypeName = "varchar(35)")]
        public string con_email1 { get; set; }

        [Column(TypeName = "varchar(35)")]
        public string con_email2 { get; set; }

        [Column(TypeName = "varchar(25)")]
        public string whatsapp_no1 { get; set; }

        [Column(TypeName = "varchar(25)")]
        public string whatsapp_no2 { get; set; }

        [Column(TypeName = "varchar(35)")]
        public string twitter_acc { get; set; }

        [Column(TypeName = "varchar(35)")]
        public string linked_in_acc { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string website_personal { get; set; }

        [Column(TypeName = "varchar(20)")]
        public string Role { get; set; }

        [Column(TypeName = "varchar(20)")]

        [Required]
        public Boolean status { get; set; }

    }
}
