﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class addedapp_job : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "app_job_tbl",
                schema: "jobportal",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    employee_id = table.Column<int>(type: "int", nullable: false),
                    job_id_fk = table.Column<int>(type: "int", nullable: false),
                    date_applied = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_job_tbl", x => x.id);
                    table.ForeignKey(
                        name: "FK_app_job_tbl_emp_tbl_employee_id",
                        column: x => x.employee_id,
                        principalSchema: "jobportal",
                        principalTable: "emp_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_app_job_tbl_job_reg_tbl_job_id_fk",
                        column: x => x.job_id_fk,
                        principalSchema: "jobportal",
                        principalTable: "job_reg_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_app_job_tbl_employee_id",
                schema: "jobportal",
                table: "app_job_tbl",
                column: "employee_id");

            migrationBuilder.CreateIndex(
                name: "IX_app_job_tbl_job_id_fk",
                schema: "jobportal",
                table: "app_job_tbl",
                column: "job_id_fk");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "app_job_tbl",
                schema: "jobportal");
        }
    }
}
