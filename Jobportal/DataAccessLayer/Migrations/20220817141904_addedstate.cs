﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class addedstate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "State_tbl",
                schema: "general",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    state_name = table.Column<string>(type: "varchar(50)", nullable: false),
                    code = table.Column<string>(type: "varchar(20)", nullable: false),
                    country_id = table.Column<int>(type: "int", nullable: false),
                    identifier = table.Column<string>(type: "varchar(50)", nullable: true),
                    is_active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_State_tbl", x => x.id);
                    table.ForeignKey(
                        name: "FK_State_tbl_Country_tbl_country_id",
                        column: x => x.country_id,
                        principalSchema: "general",
                        principalTable: "Country_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_State_tbl_country_id",
                schema: "general",
                table: "State_tbl",
                column: "country_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "State_tbl",
                schema: "general");
        }
    }
}
