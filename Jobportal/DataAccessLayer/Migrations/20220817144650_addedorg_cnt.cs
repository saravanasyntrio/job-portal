﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class addedorg_cnt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "org_cnt_tbl",
                schema: "organization",
                columns: table => new
                {
                    org_c_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    org_id = table.Column<int>(type: "int", nullable: false),
                    con_number1 = table.Column<string>(type: "varchar(25)", nullable: true),
                    con_number2 = table.Column<string>(type: "varchar(25)", nullable: true),
                    con_email1 = table.Column<string>(type: "varchar(35)", nullable: true),
                    con_email2 = table.Column<string>(type: "varchar(35)", nullable: true),
                    whatsapp_no1 = table.Column<string>(type: "varchar(25)", nullable: true),
                    whatsapp_no2 = table.Column<string>(type: "varchar(25)", nullable: true),
                    twitter_acc = table.Column<string>(type: "varchar(35)", nullable: true),
                    linked_in_acc = table.Column<string>(type: "varchar(35)", nullable: true),
                    website_address = table.Column<string>(type: "varchar(50)", nullable: true),
                    total_employee = table.Column<int>(type: "int", nullable: false),
                    total_board_mem = table.Column<int>(type: "int", nullable: false),
                    org_closed = table.Column<bool>(type: "bit", nullable: false),
                    org_closed_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    org_closed_reason = table.Column<string>(type: "varchar(100)", nullable: true),
                    org_inactive = table.Column<bool>(type: "bit", nullable: false),
                    org_inactive_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    org_inactive_reason = table.Column<string>(type: "varchar(100)", nullable: true),
                    Description = table.Column<string>(type: "varchar(250)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_org_cnt_tbl", x => x.org_c_id);
                    table.ForeignKey(
                        name: "FK_org_cnt_tbl_org_tbl1_org_id",
                        column: x => x.org_id,
                        principalSchema: "organization",
                        principalTable: "org_tbl1",
                        principalColumn: "org_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_org_cnt_tbl_org_id",
                schema: "organization",
                table: "org_cnt_tbl",
                column: "org_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "org_cnt_tbl",
                schema: "organization");
        }
    }
}
