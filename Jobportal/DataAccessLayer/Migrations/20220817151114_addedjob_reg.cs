﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class addedjob_reg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "job_reg_tbl",
                schema: "jobportal",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    job_title = table.Column<string>(type: "varchar(50)", nullable: false),
                    job_skills = table.Column<string>(type: "varchar(max)", nullable: false),
                    job_desc = table.Column<string>(type: "varchar(max)", nullable: false),
                    posting_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    start_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    end_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    state_fk = table.Column<int>(type: "int", nullable: false),
                    desired_state_fk = table.Column<int>(type: "int", nullable: false),
                    employeer_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_job_reg_tbl", x => x.id);
                    table.ForeignKey(
                        name: "FK_job_reg_tbl_emp_tbl_employeer_id",
                        column: x => x.employeer_id,
                        principalSchema: "jobportal",
                        principalTable: "emp_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_job_reg_tbl_State_tbl_desired_state_fk",
                        column: x => x.desired_state_fk,
                        principalSchema: "general",
                        principalTable: "State_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_job_reg_tbl_State_tbl_state_fk",
                        column: x => x.state_fk,
                        principalSchema: "general",
                        principalTable: "State_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_job_reg_tbl_desired_state_fk",
                schema: "jobportal",
                table: "job_reg_tbl",
                column: "desired_state_fk");

            migrationBuilder.CreateIndex(
                name: "IX_job_reg_tbl_employeer_id",
                schema: "jobportal",
                table: "job_reg_tbl",
                column: "employeer_id");

            migrationBuilder.CreateIndex(
                name: "IX_job_reg_tbl_state_fk",
                schema: "jobportal",
                table: "job_reg_tbl",
                column: "state_fk");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "job_reg_tbl",
                schema: "jobportal");
        }
    }
}
