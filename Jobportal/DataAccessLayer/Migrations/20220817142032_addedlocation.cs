﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class addedlocation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Location_tbl",
                schema: "general",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    location_name = table.Column<string>(type: "varchar(50)", nullable: false),
                    code = table.Column<string>(type: "varchar(20)", nullable: false),
                    province_area_id = table.Column<int>(type: "int", nullable: false),
                    identifier = table.Column<string>(type: "varchar(50)", nullable: true),
                    is_active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location_tbl", x => x.id);
                    table.ForeignKey(
                        name: "FK_Location_tbl_District_tbl_province_area_id",
                        column: x => x.province_area_id,
                        principalSchema: "general",
                        principalTable: "District_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Location_tbl_province_area_id",
                schema: "general",
                table: "Location_tbl",
                column: "province_area_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Location_tbl",
                schema: "general");
        }
    }
}
