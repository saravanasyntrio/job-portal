﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class addedorg_mem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "org_mem_tbl",
                schema: "organization",
                columns: table => new
                {
                    org_m_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    org_id = table.Column<int>(type: "int", nullable: false),
                    org_c_id = table.Column<int>(type: "int", nullable: false),
                    stake_holder = table.Column<string>(type: "varchar(50)", nullable: false),
                    short_name = table.Column<string>(type: "varchar(20)", nullable: true),
                    con_number1 = table.Column<string>(type: "varchar(25)", nullable: true),
                    con_number2 = table.Column<string>(type: "varchar(25)", nullable: true),
                    con_email1 = table.Column<string>(type: "varchar(35)", nullable: true),
                    con_email2 = table.Column<string>(type: "varchar(35)", nullable: true),
                    whatsapp_no1 = table.Column<string>(type: "varchar(25)", nullable: true),
                    whatsapp_no2 = table.Column<string>(type: "varchar(25)", nullable: true),
                    twitter_acc = table.Column<string>(type: "varchar(35)", nullable: true),
                    linked_in_acc = table.Column<string>(type: "varchar(35)", nullable: true),
                    website_personal = table.Column<string>(type: "varchar(50)", nullable: true),
                    Role = table.Column<string>(type: "varchar(20)", nullable: true),
                    status = table.Column<string>(type: "varchar(20)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_org_mem_tbl", x => x.org_m_id);
                    table.ForeignKey(
                        name: "FK_org_mem_tbl_org_cnt_tbl_org_c_id",
                        column: x => x.org_c_id,
                        principalSchema: "organization",
                        principalTable: "org_cnt_tbl",
                        principalColumn: "org_c_id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_org_mem_tbl_org_tbl1_org_id",
                        column: x => x.org_id,
                        principalSchema: "organization",
                        principalTable: "org_tbl1",
                        principalColumn: "org_id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_org_mem_tbl_org_c_id",
                schema: "organization",
                table: "org_mem_tbl",
                column: "org_c_id");

            migrationBuilder.CreateIndex(
                name: "IX_org_mem_tbl_org_id",
                schema: "organization",
                table: "org_mem_tbl",
                column: "org_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "org_mem_tbl",
                schema: "organization");
        }
    }
}
