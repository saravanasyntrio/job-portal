﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class addedseek : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "seek_tbl",
                schema: "jobportal",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    s_name = table.Column<string>(type: "varchar(50)", nullable: false),
                    email_id = table.Column<string>(type: "varchar(50)", nullable: false),
                    e_password = table.Column<string>(type: "varchar(50)", nullable: false),
                    gender = table.Column<string>(type: "varchar(10)", nullable: false),
                    seeker_type = table.Column<int>(type: "int", nullable: false),
                    registration_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    contact1 = table.Column<string>(type: "varchar(25)", nullable: false),
                    contact2 = table.Column<string>(type: "varchar(25)", nullable: true),
                    start_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    end_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    DOB = table.Column<string>(type: "varchar(50)", nullable: false),
                    approved_by = table.Column<int>(type: "int", nullable: false),
                    status = table.Column<bool>(type: "bit", nullable: false),
                    e_tokken = table.Column<string>(type: "varchar(50)", nullable: false),
                    last_login = table.Column<DateTime>(type: "datetime", nullable: false),
                    s_resume = table.Column<string>(type: "varchar(max)", nullable: false),
                    s_experience = table.Column<float>(type: "real", nullable: false),
                    key_skills = table.Column<string>(type: "varchar(250)", nullable: false),
                    profile_heading = table.Column<string>(type: "varchar(250)", nullable: false),
                    higher_qualification = table.Column<int>(type: "int", nullable: false),
                    allow_sms = table.Column<bool>(type: "bit", nullable: false),
                    allow_email = table.Column<bool>(type: "bit", nullable: false),
                    face_img = table.Column<string>(type: "varchar(max)", nullable: false),
                    state_fk = table.Column<int>(type: "int", nullable: false),
                    dist_fk = table.Column<int>(type: "int", nullable: false),
                    desired_state_fk = table.Column<int>(type: "int", nullable: false),
                    desired_dist_fk = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seek_tbl", x => x.id);
                    table.ForeignKey(
                        name: "FK_seek_tbl_District_tbl_desired_dist_fk",
                        column: x => x.desired_dist_fk,
                        principalSchema: "general",
                        principalTable: "District_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_seek_tbl_District_tbl_dist_fk",
                        column: x => x.dist_fk,
                        principalSchema: "general",
                        principalTable: "District_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_seek_tbl_sadmin_tbl_approved_by",
                        column: x => x.approved_by,
                        principalSchema: "jobportal",
                        principalTable: "sadmin_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_seek_tbl_State_tbl_desired_state_fk",
                        column: x => x.desired_state_fk,
                        principalSchema: "general",
                        principalTable: "State_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_seek_tbl_State_tbl_state_fk",
                        column: x => x.state_fk,
                        principalSchema: "general",
                        principalTable: "State_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_seek_tbl_approved_by",
                schema: "jobportal",
                table: "seek_tbl",
                column: "approved_by");

            migrationBuilder.CreateIndex(
                name: "IX_seek_tbl_desired_dist_fk",
                schema: "jobportal",
                table: "seek_tbl",
                column: "desired_dist_fk");

            migrationBuilder.CreateIndex(
                name: "IX_seek_tbl_desired_state_fk",
                schema: "jobportal",
                table: "seek_tbl",
                column: "desired_state_fk");

            migrationBuilder.CreateIndex(
                name: "IX_seek_tbl_dist_fk",
                schema: "jobportal",
                table: "seek_tbl",
                column: "dist_fk");

            migrationBuilder.CreateIndex(
                name: "IX_seek_tbl_state_fk",
                schema: "jobportal",
                table: "seek_tbl",
                column: "state_fk");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "seek_tbl",
                schema: "jobportal");
        }
    }
}
