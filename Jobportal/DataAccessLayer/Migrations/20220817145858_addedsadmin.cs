﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class addedsadmin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "jobportal");

            migrationBuilder.CreateTable(
                name: "sadmin_tbl",
                schema: "jobportal",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "varchar(50)", nullable: false),
                    email_id = table.Column<string>(type: "varchar(50)", nullable: false),
                    a_password = table.Column<string>(type: "varchar(50)", nullable: false),
                    employee_type = table.Column<string>(type: "varchar(25)", nullable: false),
                    contact1 = table.Column<string>(type: "varchar(25)", nullable: false),
                    contact2 = table.Column<string>(type: "varchar(25)", nullable: false),
                    start_date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    end_date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    status = table.Column<bool>(type: "bit", nullable: false),
                    a_token = table.Column<string>(type: "varchar(100)", nullable: false),
                    last_login = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sadmin_tbl", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "sadmin_tbl",
                schema: "jobportal");
        }
    }
}
