﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class addedcountry : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Country_tbl",
                schema: "general",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    country_name = table.Column<string>(type: "varchar(50)", nullable: false),
                    code = table.Column<string>(type: "varchar(50)", nullable: false),
                    currency_id = table.Column<int>(type: "int", nullable: false),
                    nationality = table.Column<string>(type: "varchar(50)", nullable: false),
                    identifier = table.Column<string>(type: "varchar(20)", nullable: true),
                    is_active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country_tbl", x => x.id);
                    table.ForeignKey(
                        name: "FK_Country_tbl_Currency_tbl_currency_id",
                        column: x => x.currency_id,
                        principalSchema: "general",
                        principalTable: "Currency_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Country_tbl_currency_id",
                schema: "general",
                table: "Country_tbl",
                column: "currency_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Country_tbl",
                schema: "general");
        }
    }
}
