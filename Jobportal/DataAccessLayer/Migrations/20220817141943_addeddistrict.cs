﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class addeddistrict : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "District_tbl",
                schema: "general",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    dist_name = table.Column<string>(type: "varchar(50)", nullable: false),
                    code = table.Column<string>(type: "varchar(20)", nullable: false),
                    state_id = table.Column<int>(type: "int", nullable: false),
                    identifier = table.Column<string>(type: "varchar(50)", nullable: true),
                    is_active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_District_tbl", x => x.id);
                    table.ForeignKey(
                        name: "FK_District_tbl_State_tbl_state_id",
                        column: x => x.state_id,
                        principalSchema: "general",
                        principalTable: "State_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_District_tbl_state_id",
                schema: "general",
                table: "District_tbl",
                column: "state_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "District_tbl",
                schema: "general");
        }
    }
}
