﻿// <auto-generated />
using DataAccessLayer;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DataAccessLayer.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20220817142032_addedlocation")]
    partial class addedlocation
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.10")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataAccessLayer.Models.Country_tbl", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("code")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("country_name")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<int>("currency_id")
                        .HasColumnType("int");

                    b.Property<string>("identifier")
                        .HasColumnType("varchar(20)");

                    b.Property<bool>("is_active")
                        .HasColumnType("bit");

                    b.Property<string>("nationality")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("id");

                    b.HasIndex("currency_id");

                    b.ToTable("Country_tbl", "general");
                });

            modelBuilder.Entity("DataAccessLayer.Models.Currency_tbl", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("code")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<string>("currency_name")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<bool>("is_active")
                        .HasColumnType("bit");

                    b.Property<bool>("is_default")
                        .HasColumnType("bit");

                    b.Property<bool>("symbol")
                        .HasColumnType("bit");

                    b.HasKey("id");

                    b.ToTable("Currency_tbl", "general");
                });

            modelBuilder.Entity("DataAccessLayer.Models.District_tbl", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("code")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<string>("dist_name")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("identifier")
                        .HasColumnType("varchar(50)");

                    b.Property<bool>("is_active")
                        .HasColumnType("bit");

                    b.Property<int>("state_id")
                        .HasColumnType("int");

                    b.HasKey("id");

                    b.HasIndex("state_id");

                    b.ToTable("District_tbl", "general");
                });

            modelBuilder.Entity("DataAccessLayer.Models.Location_tbl", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("code")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<string>("identifier")
                        .HasColumnType("varchar(50)");

                    b.Property<bool>("is_active")
                        .HasColumnType("bit");

                    b.Property<string>("location_name")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<int>("province_area_id")
                        .HasColumnType("int");

                    b.HasKey("id");

                    b.HasIndex("province_area_id");

                    b.ToTable("Location_tbl", "general");
                });

            modelBuilder.Entity("DataAccessLayer.Models.State_tbl", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("code")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<int>("country_id")
                        .HasColumnType("int");

                    b.Property<string>("identifier")
                        .HasColumnType("varchar(50)");

                    b.Property<bool>("is_active")
                        .HasColumnType("bit");

                    b.Property<string>("state_name")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("id");

                    b.HasIndex("country_id");

                    b.ToTable("State_tbl", "general");
                });

            modelBuilder.Entity("DataAccessLayer.Models.Testing_tbl", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("testing_name")
                        .HasColumnType("varchar(50)");

                    b.HasKey("id");

                    b.ToTable("Testing_tbl", "general");
                });

            modelBuilder.Entity("DataAccessLayer.Models.Country_tbl", b =>
                {
                    b.HasOne("DataAccessLayer.Models.Currency_tbl", "currency")
                        .WithMany()
                        .HasForeignKey("currency_id")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("currency");
                });

            modelBuilder.Entity("DataAccessLayer.Models.District_tbl", b =>
                {
                    b.HasOne("DataAccessLayer.Models.State_tbl", "state")
                        .WithMany()
                        .HasForeignKey("state_id")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("state");
                });

            modelBuilder.Entity("DataAccessLayer.Models.Location_tbl", b =>
                {
                    b.HasOne("DataAccessLayer.Models.District_tbl", "district")
                        .WithMany()
                        .HasForeignKey("province_area_id")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("district");
                });

            modelBuilder.Entity("DataAccessLayer.Models.State_tbl", b =>
                {
                    b.HasOne("DataAccessLayer.Models.Country_tbl", "country")
                        .WithMany()
                        .HasForeignKey("country_id")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("country");
                });
#pragma warning restore 612, 618
        }
    }
}
