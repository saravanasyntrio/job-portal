﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class addedorg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "org_tbl1",
                schema: "organization",
                columns: table => new
                {
                    org_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    org_name = table.Column<string>(type: "varchar(100)", nullable: false),
                    short_name = table.Column<string>(type: "varchar(20)", nullable: true),
                    parent_id = table.Column<int>(type: "int", nullable: false),
                    org_type_id = table.Column<int>(type: "int", nullable: false),
                    country_id = table.Column<int>(type: "int", nullable: false),
                    state_id = table.Column<int>(type: "int", nullable: false),
                    currency_id = table.Column<int>(type: "int", nullable: false),
                    dist_id = table.Column<int>(type: "int", nullable: false),
                    code = table.Column<string>(type: "varchar(50)", nullable: false),
                    tax_number = table.Column<string>(type: "varchar(50)", nullable: true),
                    vat_number = table.Column<string>(type: "varchar(50)", nullable: true),
                    tl_expiry_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    c_group_name = table.Column<string>(type: "varchar(50)", nullable: true),
                    comme_name = table.Column<string>(type: "varchar(50)", nullable: true),
                    cmp_img = table.Column<byte>(type: "tinyint", nullable: false),
                    cmp_logo = table.Column<byte>(type: "tinyint", nullable: false),
                    land_mark = table.Column<string>(type: "varchar(50)", nullable: true),
                    lat_loc = table.Column<string>(type: "varchar(25)", nullable: true),
                    lng_loc = table.Column<string>(type: "varchar(25)", nullable: true),
                    door_no = table.Column<string>(type: "varchar(25)", nullable: true),
                    street_name = table.Column<string>(type: "varchar(35)", nullable: true),
                    loc_name = table.Column<string>(type: "varchar(35)", nullable: true),
                    dist = table.Column<string>(type: "varchar(35)", nullable: true),
                    post_no = table.Column<string>(type: "varchar(15)", nullable: true),
                    est_year = table.Column<DateTime>(type: "datetime", nullable: false),
                    no_branch = table.Column<int>(type: "int", nullable: false),
                    status = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_org_tbl1", x => x.org_id);
                    table.ForeignKey(
                        name: "FK_org_tbl1_Country_tbl_country_id",
                        column: x => x.country_id,
                        principalSchema: "general",
                        principalTable: "Country_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_org_tbl1_Currency_tbl_currency_id",
                        column: x => x.currency_id,
                        principalSchema: "general",
                        principalTable: "Currency_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_org_tbl1_District_tbl_dist_id",
                        column: x => x.dist_id,
                        principalSchema: "general",
                        principalTable: "District_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_org_tbl1_org_cat_tbl_org_type_id",
                        column: x => x.org_type_id,
                        principalSchema: "organization",
                        principalTable: "org_cat_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_org_tbl1_State_tbl_state_id",
                        column: x => x.state_id,
                        principalSchema: "general",
                        principalTable: "State_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_org_tbl1_country_id",
                schema: "organization",
                table: "org_tbl1",
                column: "country_id");

            migrationBuilder.CreateIndex(
                name: "IX_org_tbl1_currency_id",
                schema: "organization",
                table: "org_tbl1",
                column: "currency_id");

            migrationBuilder.CreateIndex(
                name: "IX_org_tbl1_dist_id",
                schema: "organization",
                table: "org_tbl1",
                column: "dist_id");

            migrationBuilder.CreateIndex(
                name: "IX_org_tbl1_org_type_id",
                schema: "organization",
                table: "org_tbl1",
                column: "org_type_id");

            migrationBuilder.CreateIndex(
                name: "IX_org_tbl1_state_id",
                schema: "organization",
                table: "org_tbl1",
                column: "state_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "org_tbl1",
                schema: "organization");
        }
    }
}
