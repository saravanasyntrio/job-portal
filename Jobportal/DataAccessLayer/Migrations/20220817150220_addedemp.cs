﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class addedemp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "emp_tbl",
                schema: "jobportal",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    e_name = table.Column<string>(type: "varchar(50)", nullable: false),
                    email_id = table.Column<string>(type: "varchar(50)", nullable: false),
                    e_password = table.Column<string>(type: "varchar(50)", nullable: false),
                    registration_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    contact1 = table.Column<string>(type: "varchar(25)", nullable: false),
                    contact2 = table.Column<string>(type: "varchar(25)", nullable: true),
                    start_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    end_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    company_website = table.Column<string>(type: "varchar(50)", nullable: false),
                    approved_by = table.Column<int>(type: "int", nullable: false),
                    status = table.Column<bool>(type: "bit", nullable: false),
                    e_tokken = table.Column<string>(type: "varchar(100)", nullable: false),
                    last_login = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_emp_tbl", x => x.id);
                    table.ForeignKey(
                        name: "FK_emp_tbl_sadmin_tbl_approved_by",
                        column: x => x.approved_by,
                        principalSchema: "jobportal",
                        principalTable: "sadmin_tbl",
                        principalColumn: "id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_emp_tbl_approved_by",
                schema: "jobportal",
                table: "emp_tbl",
                column: "approved_by");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "emp_tbl",
                schema: "jobportal");
        }
    }
}
